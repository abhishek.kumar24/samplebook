import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sample_book_company/firebase/repository.dart';
import 'package:sample_book_company/screens/bottomBar/bottom_nav.dart';
import 'package:sample_book_company/screens/create%20portfolio/create_portfolio.dart';

import '../screens/login/login_screen.dart';

class Authentication extends StatelessWidget {
  Authentication({Key? key}) : super(key: key);

  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    if (auth.currentUser != null) {
      getcurrentuser();
      return CreatePortfolio();
    } else {
      return LoginScreen();
    }
  }
}
