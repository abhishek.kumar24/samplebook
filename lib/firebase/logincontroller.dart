import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:sample_book_company/firebase/repository.dart';
import 'package:sample_book_company/screens/bottomBar/bottom_nav.dart';
import 'package:sample_book_company/screens/create%20portfolio/create_portfolio.dart';
import 'package:sample_book_company/screens/otp/otp_screen.dart';

class LoginScreenController extends GetxController {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final TextEditingController phone = TextEditingController();
  String otp = '';
  String verificationId = "";
  bool isLoading = false;

  void verifyPhoneNumber(BuildContext context) async {
    isLoading = true;

    update();

    try {
      await _auth.verifyPhoneNumber(
        phoneNumber: "+91${phone.text}",
        timeout: Duration(seconds: 60),
        verificationCompleted: (PhoneAuthCredential credential) async {
          await _auth.signInWithCredential(credential);
          Fluttertoast.showToast(msg: "Phone Number Verifed");
        },
        verificationFailed: (FirebaseAuthException exception) {
          Fluttertoast.showToast(msg: "Verification Failed");
          Navigator.pop(context);
        },
        codeSent: (String _verificationId, int? forceRespondToken) {
          Fluttertoast.showToast(msg: "Verification code sent");

          verificationId = _verificationId;
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => OTPScreen()));
        },
        codeAutoRetrievalTimeout: (String _verificationId) {
          verificationId = _verificationId;
        },
      );
    } catch (e) {
      Fluttertoast.showToast(msg: "Error Occured : $e");
    }
  }

  void signInWithPhoneNumber(BuildContext context) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: otp,
      );

      var signInUser = await _auth.signInWithCredential(credential);

      final User? user = signInUser.user;

      Fluttertoast.showToast(msg: "Sign In Sucessfully");

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => CreatePortfolio()));

      print("Sign In Sucessfully, User UID : ${user!.phoneNumber}");
    } catch (e) {
      Fluttertoast.showToast(msg: "Error Occured : $e");
    }
  }
}
