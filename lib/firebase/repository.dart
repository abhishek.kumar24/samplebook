import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:math';

import 'package:fluttertoast/fluttertoast.dart';

String phonenumber = "";
final firestoreInstance = FirebaseFirestore.instance;

void getcurrentuser() {
  phonenumber = FirebaseAuth.instance.currentUser!.phoneNumber.toString();
}

Future<void> createportfolio(
    {String? yourname, String? businessname, String? category}) async {
  print('--------->>>$yourname, $businessname,$category,$phonenumber');
  Random random = new Random();
  int randomNumber = random.nextInt(900) + 100;
  firestoreInstance.collection("portfolio").add({
    "yourname": yourname,
    "businessname": businessname,
    "category": category,
    "phonenumber": phonenumber,
    "portfoliolink": businessname!.replaceAll(" ", "") + randomNumber.toString()
  }).catchError((error) => Fluttertoast.showToast(msg: "$error"));
}

Future<bool> checkmanaportfolio() {
  return firestoreInstance
      .collection("portfolio")
      .where("phonenumber", isEqualTo: phonenumber)
      .get()
      .then((value) {
    if (value.docs.length > 0) {
      return true;
    }
    return false;
  });
}

Map<int, dynamic> portfolio = Map();
void loadportfolio(Function ondone) {
  firestoreInstance
      .collection("portfolio")
      .where("phonenumber", isEqualTo: phonenumber)
      .get()
      .then((value) {
    // print(value );
    portfolio.clear();
    int a = 0;
    value.docs.forEach((element) {
      portfolio.addAll({
        a: {
          "businessname": element.data()['businessname'],
          "category": element.data()['category'],
          "phonenumber": element.data()['phonenumber'],
          "yourname": element.data()['yourname'],
          "portfoliolink": element.data()['portfoliolink']
          // "refid": element.id
        }
      });
      a = a + 1;
    });
    // print(categories);
    ondone();
  });
}

Future<void> addCategory(String? name, String? imageUrl) async {
  firestoreInstance.collection("category").add({
    "name": name,
    "phonenumber": phonenumber,
    "image": imageUrl,
    "time": DateTime.now()
  }).catchError((error) => Fluttertoast.showToast(msg: "$error"));
}

Map<String, dynamic> categoryList = Map();
loadCategoryList() {
  firestoreInstance
      .collection('category')
      .where('phonenumber', isEqualTo: phonenumber)
      .get()
      .then((value) {
    value.docs.forEach((element) {
      categoryList.addAll({
        "image": element.data()['image'],
        "name": element.data()['name'],
        "phonenumber": element.data()['phonenumber'],
      });
    });
  });
}
