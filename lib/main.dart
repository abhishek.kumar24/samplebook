import 'dart:async';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sample_book_company/screens/bottomBar/bottom_nav.dart';
import 'package:sample_book_company/screens/create%20portfolio/create_portfolio.dart';
import 'package:sample_book_company/screens/login/login_screen.dart';
import 'package:sample_book_company/utils/show.dart';

import 'firebase/repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp().then((value) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Sample book',
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration(seconds: 2), () {
      if (FirebaseAuth.instance.currentUser == null) {
        show(context: context, toshow: LoginScreen());
      } else {
        getcurrentuser();

        checkportfolio();
      }
    });
    super.initState();
  }

  Future checkportfolio() async {
    if (await checkmanaportfolio() == false) {
      show(context: context, toshow: CreatePortfolio());
    } else {
      loadportfolio(() async {
        await show(context: context, toshow: NavBar());
        exit(0);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xFF3d55b9), Color(0xFF3d55b9)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
          child: Center(
              child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/images/logo.png"),
              const Text('SampleBook',
                  style: TextStyle(
                    fontFamily: 'medium',
                    color: Colors.white,
                    fontSize: 32,
                  ))
            ],
          ))),
    );
  }
}
