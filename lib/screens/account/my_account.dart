import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sample_book_company/screens/login/login_screen.dart';
import 'package:sample_book_company/utils/fontstyle.dart';

class MyProfile extends StatefulWidget {
  const MyProfile({Key? key}) : super(key: key);

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  TextEditingController name = TextEditingController();
  TextEditingController category = TextEditingController();
  TextEditingController address = TextEditingController();
  String myname = 'Abhishek';
  String cat = 'test';
  String add = 'test';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    name.text = myname;
    category.text = cat;
    address.text = add;
  }

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFe4e4e4),
      appBar: AppBar(
        backgroundColor: Color(0xFF6078dc),
        title: Text(
          'My Profile',
          style: appbarTextStyle,
        ),
        centerTitle: true,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Container(
              height: 100,
              width: 100,
              color: Colors.white,
              child: SvgPicture.asset(
                'assets/images/ic_user.svg',
                height: 44,
              ),
            ),
          ),
          Text(
            'Business Name',
            style: heading,
          ),
          Text(
            '9523086530',
            style: subheading,
          ),
          customTextField(name),
          customTextField(category),
          customTextField(address),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xff6078dc),
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
                    child: Text(
                      'Edit',
                      style: appbarTextStyle,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    await _signOut();
                    Fluttertoast.showToast(msg: 'Logout!');
                    pushNewScreen(context,
                        screen: LoginScreen(), withNavBar: false);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0xff50cf5a),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8, vertical: 8),
                      child: Row(
                        children: [
                          Icon(
                            Icons.logout,
                            size: 24,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Logout',
                            style: appbarTextStyle,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget customTextField(TextEditingController cont) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
    ),
    child: TextField(
      enabled: false,
      controller: cont,
      decoration: InputDecoration(
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.transparent)),
      ),
      textAlign: TextAlign.center,
      style: subheading,
    ),
  );
}
