import 'package:flutter/material.dart';
import 'package:sample_book_company/utils/fontstyle.dart';

class AddWorkScreen extends StatefulWidget {
  const AddWorkScreen({Key? key}) : super(key: key);

  @override
  State<AddWorkScreen> createState() => _AddWorkScreenState();
}

class _AddWorkScreenState extends State<AddWorkScreen> {
  TextEditingController name = TextEditingController();
  TextEditingController category = TextEditingController();
  TextEditingController details = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFe4e4e4),
      appBar: AppBar(
        backgroundColor: Color(0xFF6078dc),
        title: Text(
          'Add Work',
          style: appbarTextStyle,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Add Sample Images(Upto 10)',
                textAlign: TextAlign.center,
                style: semititle,
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 0.6)),
                child: Icon(
                  Icons.image,
                  size: 40,
                ),
              ),
              customTextField('Work Name*', name, 1),
              customTextField('Work Category*', category, 1),
              customTextField('Details(Optional)', details, 5),
              Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xff6078dc),
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 80, vertical: 8),
                    child: Text(
                      'Finish',
                      style: appbarTextStyle,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget customTextField(String label, TextEditingController cont, int maxLine) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 16),
    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        label,
        style: semititle,
      ),
      Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: TextField(
          controller: cont,
          maxLines: maxLine,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 16),
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
          ),
          style: subheading,
        ),
      ),
    ]),
  );
}
