import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sample_book_company/screens/account/my_account.dart';
import 'package:sample_book_company/screens/add%20work/add_work.dart';
import 'package:sample_book_company/screens/help/help_screen.dart';
import 'package:sample_book_company/screens/home/home_screen.dart';
import 'package:sample_book_company/screens/sample/sample_screen.dart';

class NavBar extends StatefulWidget {
  NavBar({Key? key, this.currentIndex = 0}) : super(key: key);

  int currentIndex;

  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  PersistentTabController? _controller;
  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          'assets/images/ic_home.svg',
          height: 40,
          width: 40,
          color: Colors.green,
        ),
        inactiveIcon: SvgPicture.asset(
          'assets/images/ic_home.svg',
          height: 40,
          width: 40,
        ),
        title: ("Home"),
        iconSize: 80,
        activeColorSecondary: Colors.green,
        activeColorPrimary: const Color(0xffFF9C4E),
        inactiveColorPrimary: Colors.black,
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          'assets/images/ic_sample.svg',
          color: Colors.green,
          height: 40,
        ),
        inactiveIcon: SvgPicture.asset('assets/images/ic_sample.svg'),
        title: ("Sample"),
        iconSize: 40,
        activeColorSecondary: Colors.green,
        activeColorPrimary: const Color(0xffFF9C4E),
        inactiveColorPrimary: Colors.black,
      ),
      PersistentBottomNavBarItem(
          icon: SvgPicture.asset(
            'assets/images/ic_plus.svg',
            color: Colors.white,
            height: 40,
          ),
          inactiveIcon: SvgPicture.asset(
            'assets/images/ic_plus.svg',
            height: 40,
          ),
          title: ("Add"),
          activeColorSecondary: Colors.green,
          inactiveColorPrimary: Colors.black),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          'assets/images/ic_wp.svg',
          color: Colors.green,
          height: 40,
          width: 40,
        ),
        inactiveIcon: SvgPicture.asset(
          'assets/images/ic_wp.svg',
          height: 40,
        ),
        title: ("Help"),
        activeColorSecondary: Colors.green,
        activeColorPrimary: const Color(0xffFF9C4E),
        inactiveColorPrimary: Colors.black,
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          'assets/images/ic_user.svg',
          color: Colors.green,
          height: 40,
        ),
        inactiveIcon: SvgPicture.asset(
          'assets/images/ic_user.svg',
          height: 40,
        ),
        title: ("Account"),
        activeColorSecondary: Colors.green,
        activeColorPrimary: const Color(0xffff9c4e),
        inactiveColorPrimary: Colors.black,
      ),
    ];
  }

  List<Widget> _buildScreens() {
    return [
      const HomeScreen(),
      const SampleScreen(),
      const AddWorkScreen(),
      const HelpScreen(),
      const MyProfile(),
    ];
  }

  @override
  void initState() {
    _controller = PersistentTabController(initialIndex: widget.currentIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: Colors.white, // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: false, // Default is true.
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: const ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: const ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style15, // Choose the nav bar style with this property.
    );
  }
}
