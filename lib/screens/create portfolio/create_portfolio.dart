import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sample_book_company/firebase/repository.dart';
import 'package:sample_book_company/screens/bottomBar/bottom_nav.dart';
import 'package:sample_book_company/screens/home/home_screen.dart';
import 'package:sample_book_company/utils/fontstyle.dart';
import 'package:sample_book_company/utils/show.dart';

class CreatePortfolio extends StatefulWidget {
  const CreatePortfolio({Key? key}) : super(key: key);

  @override
  _CreatePortfolioState createState() => _CreatePortfolioState();
}

class _CreatePortfolioState extends State<CreatePortfolio> {
  TextEditingController name = TextEditingController();
  TextEditingController Business = TextEditingController();
  TextEditingController category = TextEditingController();

  @override
  void initState() {
    getcurrentuser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Create Portfolio',
                style: subtitle,
              ),
              CustomTextField(
                'Your Name',
                name,
              ),
              CustomTextField(
                'Your Business',
                Business,
              ),
              CustomTextField(
                'Category',
                category,
              ),
              OutlinedButton(
                  onPressed: () async {
                    await createportfolio(
                        businessname: Business.text,
                        category: category.text,
                        yourname: name.text);
                    loadportfolio(() async {
                      await show(context: context, toshow: NavBar());
                      exit(0);
                    });
                    Fluttertoast.showToast(
                        msg: 'Portfolio Created Successfully');
                  },
                  child: Text(
                    'Create Now',
                    style: link,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

Widget CustomTextField(String hintext, TextEditingController cont) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 16),
    child: TextField(
        controller: cont,
        decoration: InputDecoration(
          hintText: hintext,
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.blue,
            width: 0.5,
          )),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Color(0xFF565656),
            width: 0.5,
          )),
        ),
        style: subheading),
  );
}
