import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sample_book_company/firebase/repository.dart';
import 'package:sample_book_company/utils/fontstyle.dart';
import 'package:sample_book_company/utils/show.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Color(0xFFe4e4e4),
        body: SafeArea(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              height: size.height * 0.4,
              child: Stack(children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  height: size.height * 0.2,
                  width: size.width,
                  decoration: BoxDecoration(
                      color: Color(0xff6078dc),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(16),
                        bottomRight: Radius.circular(16),
                      )),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 32,
                      ),
                      Text(
                        portfolio[0]['businessname'],
                        style: title,
                      ),
                      Text(
                        phonenumber.substring(3, 13),
                        style: subtitle1,
                      )
                    ],
                  ),
                ),
                Positioned(
                  top: size.height * 0.15,
                  left: 32,
                  right: 32,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    height: size.height * 0.2,
                    width: size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            'Share Your Work Portfolio',
                            style: semititle,
                          ),
                          Text(
                            'One Click Share option on WhatsApp, Facebook, Telegram, SMS',
                            style: kTextStyle,
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: size.width * 0.5,
                                child: TextButton(
                                  onPressed: () {
                                    launchURL("https://" +
                                        "Samplebook.io/" +
                                        portfolio[0]['portfoliolink']);
                                  },
                                  child: Text(
                                    "Samplebook.io/" +
                                        portfolio[0]['portfoliolink'],
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontFamily: 'semi',
                                      color: Color(0xFF3d55b9),
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Color(0xff50cf5a),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 4),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/ic_wp.svg',
                                        width: 16,
                                        height: 16,
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        'Share',
                                        style: subtitle1,
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          )
                        ]),
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Text(
                'Dashboard',
                style: TextStyle(
                    fontFamily: 'medium',
                    fontSize: 24,
                    color: Color(0xff323232)),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                customContainer('Total Work', '40'),
                customContainer('Total Images', '80'),
              ],
            ),
            //   SizedBox(height: 32),
            //   Row(
            //     mainAxisSize: MainAxisSize.max,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: [
            //       Column(
            //         children: [
            //           Text(
            //             'Add Work',
            //             style: TextStyle(
            //                 fontFamily: 'medium',
            //                 fontSize: 24,
            //                 color: Color(0xff323232)),
            //           ),
            //           Container(
            //             margin: EdgeInsets.only(top: 16),
            //             decoration: BoxDecoration(
            //               color: Colors.white,
            //               borderRadius: BorderRadius.circular(5),
            //             ),
            //             child: Padding(
            //               padding: const EdgeInsets.symmetric(
            //                   horizontal: 16, vertical: 12),
            //               child: SvgPicture.asset(
            //                 'assets/images/ic_plus.svg',
            //                 height: 44,
            //                 width: 44,
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //     ],
            //   )
            // ],
          ]),
        ));
  }
}

Widget customContainer(String title, String value) {
  return Expanded(
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      padding: EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Text(
            title,
            style: heading,
          ),
          Text(
            value,
            style: subheading,
          )
        ],
      ),
    ),
  );
}
