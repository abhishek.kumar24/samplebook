import 'package:flutter/material.dart';
import 'package:sample_book_company/firebase/logincontroller.dart';
import 'package:sample_book_company/screens/otp/otp_screen.dart';
import 'package:sample_book_company/utils/fontstyle.dart';
import 'package:get/get.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _mobile = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(LoginScreenController());
    return Scaffold(
        backgroundColor: Colors.white,
        body: GetBuilder<LoginScreenController>(
            init: LoginScreenController(),
            builder: (value) {
              return controller.isLoading
                  ? Center(child: CircularProgressIndicator())
                  : SafeArea(
                      child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text('Welcome,', style: titleText),
                              Text('Login to Continue...', style: subtitle)
                            ],
                          ),
                          Center(
                              child: Image.asset("assets/images/ic_logo.png")),
                          TextField(
                              controller: controller.phone,
                              maxLength: 10,
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                label: Text('Phone Number'),
                                prefix: Text('+91'),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                  color: Colors.blue,
                                  width: 0.5,
                                )),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                  color: Color(0xFF565656),
                                  width: 0.5,
                                )),
                              ),
                              style: subheading),
                          Center(
                            child: OutlinedButton(
                              onPressed: () {
                                controller.verifyPhoneNumber(context);
                              },
                              child: const Text('Send OTP',
                                  style: TextStyle(
                                    fontFamily: 'regular',
                                    color: Color(0xFF3d55b9),
                                    fontSize: 16,
                                  )),
                            ),
                          )
                        ],
                      ),
                    ));
            }));
  }
}
