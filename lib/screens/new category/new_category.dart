import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sample_book_company/firebase/repository.dart';
import 'package:sample_book_company/utils/fontstyle.dart';
import 'package:uuid/uuid.dart';

class AddNewCategory extends StatefulWidget {
  const AddNewCategory({Key? key}) : super(key: key);

  @override
  State<AddNewCategory> createState() => _AddNewCategoryState();
}

class _AddNewCategoryState extends State<AddNewCategory> {
  TextEditingController name = TextEditingController();
  bool isLoading = false;

  final picker = ImagePicker();
  File? _imageFile;
  FirebaseStorage storage = FirebaseStorage.instance;

  Future pickImage() async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 85);

    setState(() {
      _imageFile = File(pickedFile!.path);
    });
  }

  Future<void> _uploadImage(String cateName) async {
    var uuid = Uuid().v1();
    try {
      setState(() {
        isLoading = true;
      });
      await storage.ref().child('category$uuid').putFile(
            _imageFile!,
          );
      String downloadURL =
          await FirebaseStorage.instance.ref("category$uuid").getDownloadURL();
      print('==========>$downloadURL');
      addCategory(cateName, downloadURL);
      setState(() {
        isLoading = false;
      });
    } on FirebaseException catch (error) {
      print(error);
      Fluttertoast.showToast(msg: "Something Went Wrong!");
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFe4e4e4),
      appBar: AppBar(
        backgroundColor: Color(0xFF6078dc),
        title: Text(
          'Add Category',
          style: appbarTextStyle,
        ),
        centerTitle: true,
      ),
      body: isLoading == false
          ? Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Add Category Images',
                    textAlign: TextAlign.center,
                    style: semititle,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      InkWell(
                        onTap: () => pickImage(),
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border:
                                  Border.all(color: Colors.black, width: 0.6)),
                          child: Icon(
                            Icons.image,
                            size: 40,
                          ),
                        ),
                      ),
                      SizedBox(width: 16),
                      _imageFile != null
                          ? Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.black, width: 0.6)),
                              child: Image.file(_imageFile!),
                            )
                          : Container(),
                    ],
                  ),
                  customTextField('Category Name', name),
                  Spacer(),
                  Center(
                    child: InkWell(
                      onTap: () async {
                        await _uploadImage(name.text);
                        Navigator.pop(context);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xff6078dc),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 80, vertical: 8),
                          child: Text(
                            'Add',
                            style: appbarTextStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          : Center(child: CircularProgressIndicator()),
    );
  }
}

Widget customTextField(String label, TextEditingController cont) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 16),
    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        label,
        style: semititle,
      ),
      Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: TextField(
          controller: cont,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 16),
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
          ),
          style: subheading,
        ),
      ),
    ]),
  );
}
