import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:otp_text_field/otp_text_field.dart';
import 'package:sample_book_company/firebase/logincontroller.dart';
import 'package:sample_book_company/utils/fontstyle.dart';
import 'package:otp_text_field/style.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({Key? key}) : super(key: key);

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final interval = const Duration(seconds: 1);
  final int timerMaxSeconds = 60;
  bool isVisibleTime = true;
  int currentSeconds = 0;
  String otp = "";

  String get timerText =>
      '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')}: ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';
  startTimeout([int? milliseconds]) {
    var duration = interval;

    Timer.periodic(duration, (timer) {
      setState(() {
        currentSeconds = timer.tick;

        if (timer.tick >= timerMaxSeconds) timer.cancel();
      });
    });
  }

  @override
  void initState() {
    startTimeout();
    Timer(
        Duration(seconds: timerMaxSeconds),
        () => setState(() {
              isVisibleTime = false;
            }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    LoginScreenController controller = Get.find();
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(0, 48, 0, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        'OTP Verification',
                        style: titleText,
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const Text('Enter the OTP sent to', style: subheading),
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(8, 0, 0, 0),
                      child: Text(controller.phone.text, style: subheading),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(0, 32, 0, 0),
                  child: OTPTextField(
                      length: 6,
                      width: MediaQuery.of(context).size.width,
                      textFieldAlignment: MainAxisAlignment.start,
                      fieldWidth: MediaQuery.of(context).size.width / 8,
                      fieldStyle: FieldStyle.box,
                      onChanged: (value) {
                        controller.otp = value;
                      },
                      onCompleted: (value) {
                        print(value);
                      },
                      style: subheading),
                ),
                isVisibleTime == true
                    ? Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(0, 16, 0, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Text('Seconds remaining', style: subheading),
                            Padding(
                              padding:
                                  EdgeInsetsDirectional.fromSTEB(8, 0, 0, 0),
                              child: Text(timerText, style: subheading),
                            ),
                          ],
                        ),
                      )
                    : Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(8, 16, 0, 0),
                        child: OutlinedButton(
                          onPressed: () {
                            controller.verifyPhoneNumber(context);
                          },
                          child: Text('Resend',
                              style: TextStyle(
                                fontFamily: 'regular',
                                color: Color(0xFF3d55b9),
                                fontSize: 16,
                              )),
                        ),
                      ),
                const Spacer(),
                isVisibleTime == true
                    ? Column(
                        children: <Widget>[
                          OutlinedButton(
                            onPressed: () {
                              controller.signInWithPhoneNumber(context);
                            },
                            child: Text('Verify',
                                style: TextStyle(
                                  fontFamily: 'regular',
                                  color: Color(0xFF3d55b9),
                                  fontSize: 16,
                                )),
                          ),
                        ],
                      )
                    : Container(),
                const Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
