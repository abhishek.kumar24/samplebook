import 'package:flutter/material.dart';
import 'package:sample_book_company/screens/sample/samplecategory.dart';
import 'package:sample_book_company/screens/sample/samplework.dart';
import 'package:sample_book_company/utils/fontstyle.dart';

class SampleScreen extends StatefulWidget {
  const SampleScreen({Key? key}) : super(key: key);

  @override
  _SampleScreenState createState() => _SampleScreenState();
}

class _SampleScreenState extends State<SampleScreen> {
  get title => null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Color(0xFFe4e4e4),
          appBar: AppBar(
            backgroundColor: Color(0xFF6078dc),
            elevation: 0,
            bottom: TabBar(
              indicatorColor: Colors.white,
              labelStyle: title,
              tabs: [
                Tab(
                  text: "Work",
                ),
                Tab(
                  text: "Category",
                ),
              ],
            ),
            title: Text('Samples', style: appbarTextStyle),
            centerTitle: true,
          ),
          body: TabBarView(
            children: [SampleWork(), SampleCategory()],
          ),
        ),
      ),
    );
  }
}
