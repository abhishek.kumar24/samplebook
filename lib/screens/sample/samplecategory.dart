import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sample_book_company/firebase/repository.dart';
import 'package:sample_book_company/screens/new%20category/new_category.dart';
import 'package:sample_book_company/utils/fontstyle.dart';
import 'package:intl/intl.dart';

class SampleCategory extends StatefulWidget {
  const SampleCategory({Key? key}) : super(key: key);

  @override
  _SampleCategoryState createState() => _SampleCategoryState();
}

class _SampleCategoryState extends State<SampleCategory> {
  final Stream<QuerySnapshot> categoryStream =
      FirebaseFirestore.instance.collection('category').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: categoryStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            Fluttertoast.showToast(msg: "Error to loading list");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          final List catdocs = [];
          snapshot.data?.docs.map((DocumentSnapshot document) {
            Map a = document.data() as Map<String, dynamic>;
            catdocs.add(a);
            a['id'] = document.id;
            print(catdocs);
          }).toList();
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    itemCount: catdocs.length,
                    itemBuilder: (context, index) {
                      final Timestamp timestamp =
                          catdocs[index]['time'] as Timestamp;
                      final DateTime dateTime = timestamp.toDate();
                      final dateString = DateFormat('M-d-y').format(dateTime);
                      return Container(
                        height: MediaQuery.of(context).size.height * 0.17,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                        margin: EdgeInsets.symmetric(vertical: 8),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.12,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.13,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color:
                                                  Colors.black.withOpacity(0.5),
                                              width: 0.5)),
                                      child: Center(
                                          child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Image.network(
                                            catdocs[index]['image']),
                                      )),
                                    )
                                  ],
                                ),
                                SizedBox(width: 16),
                                Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      catdocs[index]['name'],
                                      style: title1,
                                    ),
                                    Text(
                                      dateString,
                                      style: title1,
                                    ),
                                    Text(
                                      'Total Work: 15',
                                      style: title1,
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 32),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () => pushNewScreen(context,
                          screen: AddNewCategory(), withNavBar: false),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Color(0xFF6078dc)),
                          child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 32, vertical: 12),
                              child: Text(
                                'Add New Category',
                                style: title,
                              ))),
                    ),
                  ],
                ),
              )
            ],
          );
        });
  }
}
