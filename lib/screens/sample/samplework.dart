import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sample_book_company/screens/add%20work/add_work.dart';
import 'package:sample_book_company/utils/fontstyle.dart';

class SampleWork extends StatefulWidget {
  const SampleWork({Key? key}) : super(key: key);

  @override
  _SampleWorkState createState() => _SampleWorkState();
}

class _SampleWorkState extends State<SampleWork> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              itemCount: 4,
              itemBuilder: (context, index) {
                return Container(
                  height: MediaQuery.of(context).size.height * 0.217,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.12,
                                width:
                                    MediaQuery.of(context).size.height * 0.13,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                        color: Colors.black.withOpacity(0.5),
                                        width: 0.5)),
                                child: Center(
                                    child: Image.asset(
                                        'assets/images/ic_logo.png')),
                              )
                            ],
                          ),
                          SizedBox(width: 16),
                          Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Work Name',
                                style: title1,
                              ),
                              Text(
                                'Date: 12/02/2022',
                                style: title1,
                              ),
                              Text(
                                'Category Name: Name',
                                style: title1,
                              )
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.black.withOpacity(0.5),
                        thickness: 1,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            children: [Icon(Icons.image), Text('10')],
                          ),
                          Text(
                            'Share this work',
                            style: title1,
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 32),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () => pushNewScreen(context,
                    screen: AddWorkScreen(), withNavBar: false),
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFF6078dc)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 32, vertical: 12),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            'assets/images/ic_plus.svg',
                            height: 24,
                            width: 24,
                            color: Colors.white,
                          ),
                          SizedBox(width: 8),
                          Text(
                            'Add New Work',
                            style: title,
                          )
                        ],
                      ),
                    )),
              ),
            ],
          ),
        )
      ],
    );
  }
}
