import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const titleText = TextStyle(
  fontFamily: 'medium',
  color: Color(0xFF3d55b9),
  fontSize: 32,
);
const subtitle = TextStyle(
  fontFamily: 'regular',
  color: Color(0xFF3d55b9),
  fontSize: 24,
);

const heading = TextStyle(
  fontFamily: 'regular',
  color: Color(0xFF323232),
  fontSize: 16,
);

const subheading = TextStyle(
  fontFamily: 'regular',
  color: Color(0xFF323232),
  fontSize: 14,
);

const title = TextStyle(
  fontFamily: 'semi',
  color: Colors.white,
  fontSize: 14,
);

const link = TextStyle(
  fontFamily: 'semi',
  color: Color(0xFF3d55b9),
  fontSize: 14,
);

const title1 = TextStyle(
  fontFamily: 'regular',
  color: Color(0xFF3d55b9),
  fontSize: 14,
);

const semititle = TextStyle(
  fontFamily: 'medium',
  color: Color(0xff323232),
  fontSize: 14,
);
const subtitle1 = TextStyle(
  fontFamily: 'regular',
  color: Colors.white,
  fontSize: 12,
);
const appbarTextStyle = TextStyle(
  fontFamily: 'regular',
  fontSize: 18,
  color: Colors.white,
);

const kTextStyle = TextStyle(
  fontFamily: 'regular',
  color: Color(0xFF323232),
  fontSize: 12,
);
