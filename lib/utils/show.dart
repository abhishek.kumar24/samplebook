import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

Future<dynamic> show({BuildContext? context, Widget? toshow}) {
  return Navigator.of(context!).push(PageRouteBuilder(
      opaque: false,
      transitionDuration: Duration(seconds: 1),
      transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
      ) {
        return SlideTransition(
          position: new Tween<Offset>(
            begin: const Offset(1, 0.0),
            end: Offset.zero,
          ).animate(CurvedAnimation(parent: animation, curve: Curves.ease)),
          child: child,
        );
      },
      pageBuilder: (BuildContext context, Animation animation,
              Animation secondaryAnimation) =>
          toshow!));
}

launchURL(String url) async {
  url = '$url';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw Fluttertoast.showToast(msg: 'Something went wrong!');
  }
}
